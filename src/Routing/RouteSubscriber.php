<?php

namespace Drupal\section_lock\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Class RouteSubscriber.
 *
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    $layout_routes = [
      'layout_builder.remove_section' => 'delta',
      'layout_builder.choose_block' => 'delta',
      'layout_builder.add_block' => 'delta',
      'layout_builder.update_block' => 'delta',
      'layout_builder.remove_block' => 'delta',
      'layout_builder.move_block' => 'delta_from',
    ];

    foreach ($layout_routes as $layout_route => $key) {
      if ($route = $collection->get($layout_route)) {
        $route->setRequirement('_section_lock', 'TRUE');
        $route->setOption('_section_key', $key);
      }
    }
  }
}
