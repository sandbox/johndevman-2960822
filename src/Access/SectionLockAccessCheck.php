<?php

namespace Drupal\section_lock\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\layout_builder\Section;
use Drupal\layout_builder\SectionStorageInterface;

/**
 * Provides an access check for Section lock.
 */
class SectionLockAccessCheck implements AccessInterface {

  public function access(RouteMatchInterface $route_match, AccountInterface $account) {
    /** @var SectionStorageInterface $section_storage */
    $section_storage = $route_match->getParameter('section_storage');

    $routeObject = $route_match->getRouteObject();

    $delta_key = $routeObject->getOption('_section_key');
    $delta = $route_match->getParameter($delta_key);

    /** @var Section $section */
    $section = $section_storage->getSection($delta);

    $layoutSettings = $section->getLayoutSettings();

    if (array_key_exists('locked', $layoutSettings) && $layoutSettings['locked'] === 0) {
      $access = AccessResult::allowed();
    }
    else {
      $access = AccessResult::forbidden();
    }

    return $access->addCacheableDependency($section_storage);
  }

}
